describe('My Confirm application', () => {
  it('should confirm', async () => {
    await browser.url(`http://localhost:5500`);
    await $('#testButton').click();
    await browser.pause(1000);
    await browser.acceptAlert();
    await expect($('#result')).toHaveTextContaining('Confirmed');
    await browser.pause(3000);
  });
});
