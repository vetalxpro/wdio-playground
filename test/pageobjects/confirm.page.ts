import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
  /**
   * define selectors using getter methods
   */
  public get testButton() {
    return $('#testButton');
  }

  public get resultContainer() {
    return $('#result');
  }

  /**
   * a method to encapsule automation code to interact with the page
   * e.g. to login using username and password
   */
  public async confirmAction() {
    await this.testButton.click();
    await browser.pause(1000);
    await browser.acceptAlert();
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  public open() {
    return browser.url(`http://localhost:5500`);
  }
}

export default new LoginPage();
